const express = require("express");
const router = express.Router();
const { getDocument, addDocument, updateDocument, deleteDocument } = require('../util');
const { connect, disconnect } = require('../connection');
const { userSchema, profileSchema } = require('../schemas');
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const checkAuth = require("../auth/check-auth");
const { USER, TOKEN_SECRET_KEY, PROFILE } = require('../constants');

router.post("/signup", async (req, res) => {
  await connect();
  const data = await getDocument({ collection: USER, schema: userSchema, query: { email: req.body.email } });
  if (data.length) {
    res.status(400).json({ message: "User already exists." });
    await disconnect();
  }
  else {
    bcrypt.hash(req.body.password, 10, async (err, hash) => {
      if (err) {
        await disconnect();
        res.status(500).json({ error: err });
        return;
      }

      let query = {
        name: req.body.name,
        email: req.body.email,
        password: hash,
        status: req.body.status,
        userType: req.body.userType,
        manager: req.body.managerId || null
      };
      const result = await addDocument({ collection: USER, schema: userSchema, query });
      res.status(200).json({ doc: result, message: "success" });
      await disconnect();

    });
  }
});

router.put("/change-password/:id", checkAuth, async (req, res) => {
  let id = req.params.id;
  await connect();
  const doc = await getDocument({ collection: USER, schema: userSchema, query: { email: req.body.email } });
  if (!doc.length) {
    res.status(401).json({ message: "Email doesn't exist." });
    await disconnect();
    return;
  }
  const verified = await bcrypt.compare(req.body.password, doc[0].password);

  if (!verified) {
    res.status(401).json({
      message: "Wrong old password."
    });
    await disconnect();
    return;
  }
  bcrypt.hash(req.body.newPassword, 10, async (err, hash) => {
    if (err) {
      await disconnect();
      res.json({ error: err });
      return;

    }
    const result = await updateDocument({ collection: USER, schema: userSchema, query: { _id: id }, update: { password: hash } });
    res.status(200).json({ doc: result, message: "success" });

  });
});

router.post("/login", async (req, res) => {
  await connect();
  const doc = await getDocument({ collection: USER, schema: userSchema, query: { email: req.body.email }, refs: [{ path: 'manager', refCollection: USER, refSchema: userSchema, fields: ['name', 'email'] }] });
  if (!doc.length) {
    res.status(401).json({ message: "Email not recognized." });
    await disconnect();
    return;
  }

  const verified = await bcrypt.compare(req.body.password, doc[0].password);

  if (!verified) {
    res.status(401).json({
      message: "Invalid Credentials."
    });
    await disconnect();
    return;
  }

  if (doc[0].status === "pending") {
    res.status(200).json({ message: "Waiting for Approval." });
    return;
  }

  const token = jwt.sign(
    {
      email: doc[0].email,
      userId: doc[0]._id,
      name: doc[0].name
    },
    TOKEN_SECRET_KEY
    // {
    //   expiresIn: "10h"
    // }
  );
  res.status(200).json({
    message: "authorized",
    token: token,
    payload: {
      userId: doc[0]._id,
      email: doc[0].email,
      name: doc[0].name,
      userType: doc[0].userType,
      manager: doc[0].manager
    }
  });
  await disconnect();
});

router.get("/pending/:_id", checkAuth, async (req, res) => {
  await connect();
  const data = await getDocument({ collection: USER, schema: userSchema, query: { status: "pending", manager: req.params._id } });
  if (data) {
    res.status(200).json({ doc: data, message: "pending users" });
  }
  await disconnect();
});
router.get("/managerList", async (req, res) => {
  await connect();
  const data = await getDocument({ collection: USER, schema: userSchema, query: { userType: "manager" } });
  const formattedData = data.map(user => { return { _id: user._id, name: user.name } });
  if (data) {
    res.status(200).json({ doc: formattedData, message: "list of managers" });
  }
  await disconnect();
});

router.get("/active/:_id", checkAuth, async (req, res) => {
  await connect();
  const data = await getDocument({ collection: USER, schema: userSchema, query: { status: "approved", manager: req.params._id } });
  if (data) {
    res.status(200).json({ doc: data, message: "success" });
  }
  await disconnect();
});
router.get("/requests/:_id", checkAuth, async (req, res) => {
  await connect();
  const data = await getDocument({ collection: USER, schema: userSchema, query: { manager: req.params._id } });
  const pendingRequests = data.filter(request => request.status === "pending");
  const approvedRequests = data.filter(request => request.status === "approved");
  if (data) {
    res.status(200).json({ pendingRequests, approvedRequests, message: "success" });
  }
  await disconnect();
});
router.put("/request/:_id/:managerId", checkAuth, async (req, res) => {
  await connect();
  const result = await updateDocument({ collection: USER, schema: userSchema, query: { _id: req.params._id }, update: { status: "approved" } });
  const requests = await getDocument({ collection: USER, schema: userSchema, query: { manager: req.params.managerId } });
  const pendingRequests = requests.filter(request => request.status === "pending");
  const approvedRequests = requests.filter(request => request.status === "approved");
  await addDocument({ collection: PROFILE, schema: profileSchema, query: { user: req.params._id } });
  if (result)
    res.status(200).json({ pendingRequests, approvedRequests, message: "success" });
  await disconnect();
});

// router.put("/:_id", checkAuth, async (req, res) => {
//   await connect();
//   const result = await updateDocument("user", userSchema, { _id: req.params._id }, { name: req.body.name, email: req.body.email });
//   if (result)
//     res.status(200).json({ doc: result, message: "user updated" });
//   await disconnect();
// });

router.delete("/request/:_id/:managerId", checkAuth, async (req, res, next) => {
  await connect();
  const result = await deleteDocument({ collection: USER, schema: userSchema, query: { _id: req.params._id } });
  const requests = await getDocument({ collection: USER, schema: userSchema, query: { manager: req.params.managerId } });
  const pendingRequests = requests.filter(request => request.status === "pending");
  const approvedRequests = requests.filter(request => request.status === "approved");
  if (result)
    res.status(200).json({ pendingRequests, approvedRequests, message: "success" });
  await disconnect();
});

router.delete("/:_id", checkAuth, async (req, res, next) => {
  await connect();
  const profileResult = await deleteDocument({collection: PROFILE, schema: profileSchema, query: { user: req.params._id}});
  const userResult = await deleteDocument({ collection: USER, schema: userSchema, query: { _id: req.params._id } });
  if (profileResult && userResult)
    res.status(200).json({ doc: {profileResult, userResult}, message: "success" });
  await disconnect();
});


// router.get("/name/:id", checkAuth, async (req, res) => {
//   await connect();
//   const data = await getDocument({collection:"user", schema:userSchema, query:{ _id: req.params.id }});
//   if (data) {
//     res.status(200).json({ doc: data, message: "success" });
//   }
//   await disconnect();
// });

module.exports = router;
