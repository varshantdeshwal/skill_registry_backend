const express = require("express");
const router = express.Router();
const { getDocument, addDocument, updateDocument, deleteDocument } = require('../util');
const { connect, disconnect } = require('../connection');
const { profileSchema, userSchema, skillSchema } = require('../schemas');
const checkAuth = require("../auth/check-auth");
const { USER, PROFILE, SKILL } = require('../constants');

router.post("/", checkAuth, async (req, res) => {
    await connect();

    let query = {
        user: req.body.userId
    };
    const result = await addDocument({ collection: PROFILE, schema: profileSchema, query });
    res.status(200).json({ doc: result, message: "success" });
    await disconnect();

});
router.put("/:_id",checkAuth, async (req, res) => {
    await connect();
    const result = await updateDocument({ collection: PROFILE, schema: profileSchema, query: { _id: req.params._id }, update: req.body });
    if (req.body.skills) {
        const currentSkills = await getDocument({ collection: SKILL, schema: skillSchema, query: {} });
        const data = [];
        await req.body.skills.forEach(skill => {
            if (!currentSkills.some(existingSkill => existingSkill.name === skill))
                data.push({ name: skill });
        })
        await addDocument({ collection: SKILL, schema: skillSchema, query: data });
    }
    if (result)
        res.status(200).json({ doc: result, message: "success" });
    await disconnect();
});
router.get("/skills", checkAuth, async (req, res) => {
    await connect();
    const data = await getDocument({ collection: SKILL, schema: skillSchema, query: {}});
    if (data) {
        res.status(200).json({ doc: data, message: "success" });
    }
    await disconnect();
});
router.get("/:userId", checkAuth, async (req, res) => {
    await connect();
    const data = await getDocument({ collection: PROFILE, schema: profileSchema, query: { user: req.params.userId }, refs: [{ path: 'user', refCollection: USER, refSchema: userSchema, fields: ['name', 'email', 'userType'] }] });
    if (data) {
        res.status(200).json({ doc: data, message: "user profile" });
    }
    await disconnect();
});
router.post("/filter", checkAuth, async (req, res) => {
    const {
        name,
        email,
        skills,
        currentProject
    } = req.body;

    await connect();

    let profiles = [];
    let profileQuery = {};
    let userQuery = {};
    let ids = [];

    if (name) userQuery['name'] = { $regex: new RegExp(name), $options: 'i' };
    if (email) userQuery['email'] = { $regex: new RegExp(email), $options: 'i' };

    if (name || email) {
        const users = await getDocument({
            collection: USER,
            schema: userSchema,
            query: userQuery
        });

        ids = await users.map(each => {
            return { user: each._id }
        });
        if (ids.length) profileQuery['$or'] = ids;
    }

    if (skills.length) profileQuery['skills'] = { $in: skills };
    if (currentProject) profileQuery['currentProject'] = { $regex: new RegExp(currentProject), $options: 'i' };

    const filterFlag = ((name || email) && ids.length ) || (!(name || email) && !ids.length);

    if (filterFlag) {
        profiles = await getDocument({
            collection: PROFILE,
            schema: profileSchema,
            query: profileQuery,
            refs: [{
                path: 'user',
                refCollection: USER,
                refSchema: userSchema,
                fields: ['name', 'email', 'userType']
            }]
        });
    }

    if (profiles) {
        res.status(200).json({ doc: profiles, message: "success" });
    }
    await disconnect();
});
router.delete("/:userId", checkAuth, async (req, res, next) => {
    await connect();
    const result = await deleteDocument({ collection: PROFILE, schema: profileSchema, query: { user: req.params.userId } });
    if (result)
        res.status(200).json({ doc: result, message: "success" });
    await disconnect();
});


module.exports = router;