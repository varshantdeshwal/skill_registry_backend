var mongoose = require("mongoose");
const {USER} = require('../constants');

const Schema =mongoose.Schema;
const userSchema = Schema({
  manager: { type: Schema.Types.ObjectId, ref: USER},
  name: { type: String, required: true },
  email: { type: String, required: true },
  password: { type: String, required: true },
  userType: {type:String, required:true},
  status: {type: String, required: true}
});

module.exports = userSchema;