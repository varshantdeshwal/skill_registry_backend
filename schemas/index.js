const userSchema =require('./userSchema');
const profileSchema = require('./profileSchema');
const skillSchema = require ('./skillSchema');

module.exports = {
    userSchema,
    profileSchema,
    skillSchema
}