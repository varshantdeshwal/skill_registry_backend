var mongoose = require("mongoose");
const {USER} = require('../constants');

const Schema = mongoose.Schema;
const profileSchema = Schema({
    user: { type: Schema.Types.ObjectId, ref: USER },
    skills: [],
    contact:  Number,
    currentProject: String,
    location: String
});

module.exports = profileSchema;