var mongoose = require("mongoose");

const Schema = mongoose.Schema;
const skillSchema = Schema({
    name: { type : String, required : true}
});

module.exports = skillSchema;