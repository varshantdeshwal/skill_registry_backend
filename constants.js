const TOKEN_SECRET_KEY = 'decipher';
const USER = 'user';
const PROFILE = 'profile';
const SKILL = 'skill';

module.exports = {
    TOKEN_SECRET_KEY,
    USER,
    PROFILE,
    SKILL
}