const express = require("express");
const app = express();
const {user:userRoutes, profile:profileRoutes} = require("./routes");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
  res.header("Access-Control-Allow-Headers", "Content-Type, Authorization");
  next();
});

app.use("/user", userRoutes);
app.use('/profile', profileRoutes);
const port = process.env.PORT || 3048;

app.listen(port, () => console.log(`listening at ${port}`));