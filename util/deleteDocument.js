const { getModel } = require('../model');

const deleteDocument = async ({collection, schema, query}) => {
    const model = getModel({ collection, schema });
    return await model.remove(query);
};

module.exports = deleteDocument;