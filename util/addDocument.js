const {getModel}=require('../model');
const addDocument = async ({collection,schema,query}) => {
    const model=getModel({collection,schema});
   return await  model.create(query);
  };
  module.exports = addDocument;