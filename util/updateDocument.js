const { getModel } = require('../model');

const updateDocument = async ({collection, schema, query, update={}}) => {
    const model = getModel({ collection, schema });
    return await model.findOneAndUpdate(query, update);
};

module.exports = updateDocument;