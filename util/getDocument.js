const { getModel } = require('../model');

const getDocument = async ({ collection, schema, query, refs = [] }) => {
  const model = getModel({ collection, schema });
  return await model.find(query)
    .populate((refs || [])
      .map(({ path, refCollection, refSchema, fields }) => {
        const refModel = getModel({ collection: refCollection, schema: refSchema });
        return { path, model: refModel, select: fields };
      }));
};
module.exports = getDocument;