**It is an online skill registry platform for wipro employees. A platform to showcase their skills.**


**This repository contains backend code for the application with following features -**

 1. Application related endpoints(APIs).
 2. User authentication using JSON WEB TOKEN (https://github.com/auth0/node-jsonwebtoken) with each API call. 
 3. User password encryption using BCRYPT (https://www.npmjs.com/package/bcrypt).
 

**Tools and Technologies used :**


       - Mongo DB
       - Node JS
       - Express JS
	
**Local Setup -**

 **NOTE -** Make sure mongo is running in your local machine.
 
  1. Clone the repository.
  2. Run command "npm install" inside project root directory to install dependencies.
  3. Run command "node app.js" to run the application.
  


For any help, drop a mail to **varshant.14@gmail.com**