const mongoose = require("mongoose");

const connect = ()=>{
    mongoose.connect("mongodb://localhost/skill-registry");
var db = mongoose.connection;
db.on("error", function () { console.log('Mongo error'); });
db.on('connected', function () { console.log('MongoDB Connected'); });
db.on('disconnected', function () { console.log('MongoDB Disconnected'); });
}
const disconnect =()=>{
        mongoose.connection.close();
}

module.exports ={
    connect,
    disconnect
}